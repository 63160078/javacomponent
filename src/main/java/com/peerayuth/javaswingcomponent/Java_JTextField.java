/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.javaswingcomponent;

import javax.swing.JFrame;
import javax.swing.JTextField;

/**
 *
 * @author Ow
 */
public class Java_JTextField {

    public static void main(String args[]) {
        JFrame f = new JFrame("TextField Example");
        JTextField t1, t2;
        t1 = new JTextField("Write something here 1");
        t1.setBounds(50, 100, 200, 30);
        t2 = new JTextField("Write something here 2");
        t2.setBounds(50, 150, 200, 30);
        f.add(t1);
        f.add(t2);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

}
