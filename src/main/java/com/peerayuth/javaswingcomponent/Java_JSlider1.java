/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author Ow
 */
public class Java_JSlider1 extends JFrame {

    public Java_JSlider1() {
        JSlider slider = new JSlider(JSlider.HORIZONTAL, 0, 50, 25);
        JPanel panel = new JPanel();
        panel.add(slider);
        add(panel);
    }

    public static void main(String s[]) {
        Java_JSlider1 frame = new Java_JSlider1();
        frame.pack();
        frame.setVisible(true);
    }

}
