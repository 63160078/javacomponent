/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.javaswingcomponent;

import javax.swing.*;

/**
 *
 * @author Ow
 */
public class Java_JOptionPane_Alert {

    JFrame f;

    Java_JOptionPane_Alert() {
        f = new JFrame();
        JOptionPane.showMessageDialog(f, "Successfully Updated.", "Alert", JOptionPane.WARNING_MESSAGE);
    }

    public static void main(String[] args) {
        new Java_JOptionPane_Alert();
    }

}
