/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.javaswingcomponent;

import java.awt.*;
import javax.swing.JFrame;

/**
 *
 * @author Ow
 */
public class Java_JDisplayingImage extends Canvas {

    public void paint(Graphics g) {

        Toolkit t = Toolkit.getDefaultToolkit();
        Image i = t.getImage("p3.gif");
        g.drawImage(i, 120, 100, this);

    }

    public static void main(String[] args) {
        Java_JDisplayingImage m = new Java_JDisplayingImage();
        JFrame f = new JFrame();
        f.add(m);
        f.setSize(400, 400);
        f.setVisible(true);
    }

}


